---
aliases:
  - /_pages/Gallery/undirected/transparency.html
title: Partially Transparent Colors
svg: transparency.svg
copyright: Copyright © 1996 AT&T.  All rights reserved.
gv_file: transparency.gv.txt
img_src: transparency.png
url: /Gallery/undirected/transparency.html
---
This example illustrates the use of partially transparent colors for
node `fillcolor` and graph `bgcolor`.

Note the node `fillcolor` and graph `bgcolor` both have alpha/transparency
channels set.

The Graph's background is pure red, but with some transparency, showing as pink
against this page's white background.

Nodes are greener where they overlap, and you can see some pink bleeding
through.
